/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ldnr.kevin.mavenproject2hib;

import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author stag
 */
public class OffreMain {

    private static void insertions(Session session) {
        // 4 propriété R+W (nom, pays, semaine, jours)
        Destination d1 = new Destination();
        d1.setNom("L'Alhambra");
        d1.setPays("Andalousia");
        d1.setSemaines(2);
        // commence la transaction, cela desactive l'autocommit
        Transaction tx = session.beginTransaction();
        try{
            //Pour sauvegarder dans la db, il fait les insert et le setID et enregistrement dans le cache. 
            //Sans la transaction ,c'est de l'autocommit
            session.save(d1);
            session.isDirty();//False car on n'a pas fait de modif sur un objet déja save
            
            //session.save(d1);//ne fait rien, car objet deja dans le cache
            System.out.println("Votre destination 1 :" + d1);
            /*Destination d1bis = d1;
            session.save(d1bis); ne fait rien, car d1 déja dans le cache*/
            d1.setJours(d1.getJours() + 1);//fait l'update de d1
            session.isDirty();//True car on a fait une modif sur un objet déja save
            System.out.println("Votre destination 1 :" + d1);
            Destination d2 = new Destination("Porto", "Portugal", 3);
            session.save(d2);
            System.out.println("Votre destination 2 :" + d2);
            //session.clear();//vide le cache
            //session.flush();//hibernate prend en compte ce qu'il connait des objets immediatemment
            //session.evict(d2);//vide le cache de l'objet d2, d2 n'est plus suivi, hibernate ne connait plus d2
            Destination d3 = new Destination("Toulouse", "France", 8);
            System.out.println("Votre destination 3 :" + d3);
            session.save(d3);
            d3.setJours(4);
            session.delete(d3);//hibernate supprime de la db et du cache l'objet d3
            System.out.println("session : " + session.getStatistics());//Prend les infos de la session 
            //envoi du commit à la db
            tx.commit();
        }catch(Exception e){
            // Annuler tous ce qui a était fait durant la transaction (reset)
            e.printStackTrace();
            tx.rollback();
        }

    }
    public static void modifications(Session session){
        System.out.println("====fonctions modifications======");
        Transaction tx = session.beginTransaction();
        session.clear();//vide le cache de hibernate
        // Utilise le constructeur vide + les setters qui sont associés à une propriétés (ceux qui ont un @colonne)
        Destination d1 = session.load(Destination.class, 1);// le 1 correspond à l'id de l'entrée dans la base
                                                            // load envoie une exception s'il ne trouve pas l'entrée
        System.out.println("Destination 1 : "+ d1);
        d1.setJours(5);
        //session.save(d1);IL n'y a pas besoin de save plus que l'objet est déja dans la session grace au load
        
        Destination d2 = session.get(Destination.class, 2);// get envoie nul s'il ne trouve pas l'entrée
        System.out.println("Destination 2 : " + d2);
        
        Destination d41 = session.get(Destination.class, 41);// get envoie nul s'il ne trouve pas l'entrée
        System.out.println("Destination 41 : " + d41);
        
        session.flush();
        session.clear();
        
        System.out.println("Autres modifications : Update de d1");
        //session.update(d1); // 
        //session.persist(d1); // comme save mais avec exception ici, car d1 a déja un id
                               // donc on ne peut pas le recréer
        //session.refresh(d1);//équivalent de d1=session.load....
        
        
        tx.commit();
    }
    
    private static void afficheList(List<Destination> list) {
        // affiche un  tableau avec les destinations de la liste
        for (Destination d : list) {
            System.out.println(String.format("%4d %20s %12s %4d", d.getId(), d.getPays(), d.getNom(),
                    d.getJours()));
        }

    }

    private static void lecture(Session session) {
        System.out.println("**  Lectures  **");
        session.clear();// vide le cache Hibernate 
        // Outil de base 
        CriteriaBuilder cb = session.getCriteriaBuilder();
        // La requete 
        CriteriaQuery<Destination> toutesSaufPremiere = cb.createQuery(Destination.class);
        Root<Destination> table = toutesSaufPremiere.from(Destination.class);
        toutesSaufPremiere.select(table);
        toutesSaufPremiere.orderBy(cb.asc(table.get("nom")));
        toutesSaufPremiere.where(cb.gt(table.get("id"), 1));

        List<Destination> list = session.createQuery(toutesSaufPremiere).getResultList();
        System.out.println("* Criteria : ");
        afficheList(list);

        String toutesSaufPremiereHQL = "from Destination where id>1 order by nom";
        list = session.createQuery(toutesSaufPremiereHQL).getResultList();
        System.out.println("* HQL : ");
        afficheList(list);

        int maxJours = 7;
        String destinationCourtesHQL = "from Destination where jours < :j";
        list = session.createQuery(destinationCourtesHQL).setParameter("j", maxJours).getResultList();
        System.out.println("* HQL - Destination courtes : ");
        afficheList(list);
        
        int minJours = 3;
        maxJours = 8;
        String debutPays = "It";
        String destinationMoyennesHQL = "from Destination where jours >= :minj and jours < :maxj"+
                " and pays not like :p";
        list = session.createQuery(destinationMoyennesHQL).setParameter("minj", minJours).
                setParameter("maxj", maxJours).setParameter("p", debutPays+"%").getResultList();
        System.out.println("* HQL - duree moyenne et dont le pays ne  : " + "commence pas par...");
        afficheList(list);
        
        String paysHQL = "select pays from Destination";
        List <String> pays = session.createQuery(paysHQL).list();
        System.out.println("Pays : ");
        for(String p:pays){
            System.out.println("- "+p);
        }
        String nombreHQL = "select count(*) from Destination";
        Long compte = (Long) session.createQuery(nombreHQL).uniqueResult();
        System.out.println("*IL y a  : "+ compte +" destinations");
    }
    //faire des insert
    private static void insertionsPlusieursAUn(Session session){
        System.out.println("**Insertion *-1 **");
        Transaction tx = session.beginTransaction();
        Destination d1 = session.load(Destination.class, 1);
        Attraction a1 = new Attraction("Citadelle", "Collinne", 13.5);
        a1.setDestination(d1);
        session.save(a1);
        
        Attraction a2 = new Attraction("Médina", "Porte de justice", 0);
        Destination d2 = session.load(Destination.class, 2);
        a2.setDestination(d2);
        session.save(a2);
        
        Attraction a3 = new Attraction("Tram", "Centre", 1.5);
        a3.setDestination(d2);
        session.save(a3);
        
        Attraction a4 = new Attraction("Tour", "Centre", 0);
        Destination d3 = new Destination("Pise", "Italie", 2);
        a4.setDestination(d3);
        session.save(d3);//Nécéssaire
        session.save(a4);//Nécéssaire
        
        tx.commit();
    }
    
    //faire de select
    public static void lecturePLusieursAUn(Session session){
        System.out.println("**Lectures *-1 **");
        session.clear();
        Attraction a1 = session.load(Attraction.class, 1);
                                                           //hibernate va faire le select avec la jointure qu'il faut
        System.out.println("Pays de la 1ere attraction : "+a1.getDestination().getPays());
        System.out.println("statistics session "+session.getStatistics());
        
        String attParNomHQL = "from Attraction order by destination.nom";
        List<Attraction> attractions = session.createQuery(attParNomHQL).list();
        
        //afficher le nom des attractions
        System.out.println("*Toute les attractions : ");
        String destNom = null;
        for(Attraction a:attractions){
            String thisDestNom = a.getDestination().getNom();
            if(thisDestNom.equals(destNom)){
                thisDestNom="";
                System.out.println(String.format("- %15s %15s", thisDestNom, a.getNom()));
                destNom = a.getDestination().getNom();
            }
        }
        
        String maxPrixParDest = "select destination.nom, max(prix) from Attraction group by destination.id";
        List<Object[]> destEtPrix = session.createQuery(maxPrixParDest).list();
        System.out.println("*Destination et plus chere attraction : ");
        for(Object[] destEtPrixTab:destEtPrix){
            System.out.println(String.format("- %15s %.2f", destEtPrixTab[0], destEtPrixTab[1]));
        }
        
        
        String paysChersHQL = "select destination.pays from Attraction where prix > :sp";
        double seuilPrix = 10.0;
        List<String> pays = session.createQuery(paysChersHQL).setParameter("sp", seuilPrix).list();
        System.out.println("*Les pays avec une attraction chère : ");
        for(String p:pays){
            System.out.print(p+", ");
        
        }
        
        String nomPays = "%Andalousia%";
        String totalEspagneHQL = "select sum(prix) from Attraction where destination.pays like :np";
        double prixTotalEspagne = (Double)session.createQuery(totalEspagneHQL).setParameter("np", nomPays).uniqueResult();
        System.out.println(String.format("Prix total en espagne : %.02f ", prixTotalEspagne));
    }
    
    private static void insertionsPlusieursAPlusieurs(Session session) {
        System.out.println("** Insertions *-* **");
        session.clear();//vide le cache de Hibernate
        Transaction tx = session.beginTransaction();
        List<Destination> destinations = session.createQuery("from Destination order by id").list();
        // au moins :
        // - un voyage de plusieurs dates
        // - une destination dans plusieurs pays
        // - une destination non utilisée

        // ATTENTION : janvier = mois 0, février =1, donc ici on est en août avec new GregorianCalendar(2021,07,14).getTime()
        Voyage v1 = new Voyage("Merveilles Andalouses", new GregorianCalendar(2021, 07, 14).getTime());
        Voyage v2 = new Voyage("Renaissance en Italie", new GregorianCalendar(2021, 06, 3).getTime());
        v1.getDestinations().add(destinations.get(1));
        v1.getDestinations().add(destinations.get(2));
        v2.getDestinations().add(destinations.get(2));
        session.save(v1);
        session.save(v2);
        tx.commit();

        tx = session.beginTransaction();
        Voyage v3 = new Voyage("Erreur", new GregorianCalendar(2021, 06, 3).getTime());
        Destination d5 = new Destination("Erreur", "Erreur", 0);
        v3.getDestinations().add(d5);
        session.save(v3);
        session.save(d5);
        session.flush();
        session.delete(v3);//suppr le voyage v3 et l'ancienne relation entre le voyage et la table des destinations
        // => 2 deletes dans le log
        // il y a bien une suppression simple, il n'y a pas de cascade
        tx.commit();
    }

    private static void lecturesPlusieursAPlusieurs(Session session) {
        System.out.println("** Lectures *-* **");
        session.clear();//vide le cache de Hibernate
        //aficher chaque voyage avec toutes les destinations de chaque voyage

        // paresseux/LAZY : String toutHQL = "from Voyage"; 
        String toutHQL = "from Voyage fetch all properties";// normalement aurait dû forcer en Eager mais ne fonctionne pas
        //Mickael va chercher ce soir
        List<Voyage> voyages = session.createQuery(toutHQL).list();
        System.out.println("Tous les voyages et leurs destinations : ");
        for (Voyage v : voyages) {
            System.out.println(v);
            for (Destination d : v.getDestinations()) {
                System.out.println(" - " + d);
            }
        }
        // par défaut, dans les relation plusieurs à plusieurs sont liées par défaut de manière LAZY, mais on peut le forcer
        //en laison en mode EAGER (c'est l'inverse pour les liaisons un à plusieurs).

        String nomPays = "Italie";
        //String voyagesItalieHQL = "select distinct titre from Voyage where destination.pays like :np";//destination.pays n'a pas de sens
        //car destination est en fait un ensemble de destinations
        //String voyagesItalieHQL = "select distinct titre from Voyage where destinations[0].pays like :np";// ne fonctionne pas non plus car
        // même si pour nous destinations est une liste, lui ne le comprend pas comme ça
        String voyagesItalieHQL = "select distinct v.titre from Voyage as v join v.destinations as d where d.pays like :np";// LA BONNE METHODE
        //avec un join explicite pour les liaisons et alias pour les tables, souvent utilisé pour les liaisons plusieurs à plusieurs 
        List<String> titres = session.createQuery(voyagesItalieHQL).setParameter("np", nomPays).list();
        System.out.println("Titres de voyages passant par l'Italie :");
        for (String t : titres) {
            System.out.println("- " + t);
        }
    }
    
    
    
    
    
    

    public static void main(String[] args) {

        System.out.println("--Notre offre de voyages**");
        //fabriquer une configuration (prendre celles de hibernate)
        Configuration config = new Configuration();
        SessionFactory sessionFactory = config.configure().
                buildSessionFactory();
        //  on crée une session
        Session session = sessionFactory.openSession();
        // on la donne à la fonction qui en a besoin
        insertions(session);
        modifications(session);
        lecture(session);
        insertionsPlusieursAUn(session);
        lecturePLusieursAUn(session);
        insertionsPlusieursAPlusieurs(session);
        lecturesPlusieursAPlusieurs(session);
        // on ferme la session
        session.close();
    }

}
