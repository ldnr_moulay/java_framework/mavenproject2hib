/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ldnr.kevin.mavenproject2hib;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author stag
 */
@Entity 
@Table(name = "destinations")
// Toujours prendre import de javax.persistence quand on fait du jpa
// Avec le @Table, on peut renseigner le nom de la table s'il est different du nom de la classe
public class Destination implements Serializable {
    private int id;
    private String nom;
    private String pays;
    private int jours;

    

    public Destination() {
       /* nom = null;
        pays= null;
        jours=0;*/ 
       // ou cette maniere 
       this(null, null, 0);
    }

    public Destination(String nom, String pays, int jours) {
        this.id=0;
        this.nom = nom;
        this.pays = pays;
        this.jours = jours;
    }

    @Override
    public String toString() {
        return "id "+id+ nom+"("+pays+") - "+jours+"jours";
    }
    @Id @GeneratedValue(strategy =GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @Column(length = 100, nullable = false)
    public int getJours() {
        return jours;
    }
    @Column(length = 100, nullable = false)
    public String getNom() {
        return nom;
    }
    @Column(length = 100, nullable = false)
    public String getPays() {
        return pays;
    }
    @Transient
    //Pour faire en sorte que semaine ne devienne pas une colonne de destinations
    public int getSemaines() {
        return jours/7;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

   

    public void setJours(int jours) {
        if(jours<0)
            throw new IllegalArgumentException("Pas de jours < 0");
        this.jours = jours;
    }
    
     public void setSemaines(int semaines) {
       setJours(semaines*7);
    }
    
    
}
