/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ldnr.kevin.mavenproject2hib;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author stag
 */
@Entity
@Table
public class Attraction implements Serializable {
    

// Toujours prendre import de javax.persistence quand on fait du jpa
// Avec le @Table, on peut renseigner le nom de la table s'il est different du nom de la classe

        private int id;
        private String nom;
        private String lieu;
        private double prix;
        // l'id de Destination sera une clé étrangère dans la table Attraction
        private Destination destination;

        public Attraction() {
            /* nom = null;
        lieu= null;
        prix=0;*/
            // ou cette maniere 
            this(null, null, 0);
        }

        public Attraction(String nom, String lieu, double prix) {
            this.id = 0;
            this.nom = nom;
            this.lieu = lieu;
            this.prix = prix;
            destination = null;
        }

        @Override
        public String toString() {
            return "id " + id +" "+ nom + "(" + lieu + ") - " + prix + "euros";
        }

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Column(length = 100, nullable = false)
        public double getPrix() {
            return prix;
        }

        @Column(length = 100, nullable = false)
        public String getNom() {
            return nom;
        }

        @Column(length = 100, nullable = false)
        public String getLieu() {
            return lieu;
        }
/*
        @Transient
        //Pour faire en sorte que semaine ne devienne pas une colonne de destinations
        public int getSemaines() {
            return jours / 7;
        }
*/
        public void setNom(String nom) {
            this.nom = nom;
        }

        public void setLieu(String lieu) {
            this.lieu = lieu;
        }

        public void setPrix(double prix) {
            if (prix < 0) {
                throw new IllegalArgumentException("Pas de prix < 0");
            }
            this.prix = prix;
        }
    @ManyToOne(fetch = FetchType.EAGER) 
    // ManyToOne : Plusieurs attractions pour une destinations
    // xToy : x classe courante, y = classe cible
    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }
        
        
        
        
    
    
}
