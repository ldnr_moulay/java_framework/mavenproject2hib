/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ldnr.kevin.mavenproject2hib;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *L'offre commercial d'un voyage
 * @author stag
 */
@Entity
@Table(name = "voyages")
public class Voyage {

    private int id;
    private String titre;
    private Date depart;
    private List<Destination> destinations;

    public Voyage() {
        this(null, null);
    }

    public Voyage(String titre, Date depart) {
        this.id = 0;
        this.titre = titre;
        this.depart = depart;
        this.destinations = new ArrayList<>();
    }

    @Override
    public String toString() {
        return id + " " + titre + " (" + depart + ") ";
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(length = 100, nullable = false, unique = true)
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)// TemporalType.TIMESTAMP stocke date, heures, minutes et secondes, TemporalType.DATE stocke juste l'heure
    //TemporalType.TIME stocke juste heures, minutes et secondes
    public Date getDepart() {
        return depart;
    }

    public void setDepart(Date depart) {
        this.depart = depart;
    }

    @ManyToMany(cascade = {CascadeType.REMOVE}) // un voyage aura 0 ou plusieurs destinations et une destination aura 0 ou plusieurs voyages
    // création d'une table d'associations
    // s'il y a des opérations en cascade, si on supprime un objet, Hibernate supprime les objets attachés
    // après cascade => si delete de v3 => delete de la destination d5 + delete des liens entre v3 et d5
    //donc en tout, on a 3 deletes
    // si on utilise detach, c'est quand au lieu de delete on fait evict
    public List<Destination> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<Destination> destinations) {
        this.destinations = destinations;
    }
    
    
    
    
    
    
}
